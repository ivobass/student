#include <algorithm>

#include "ClassRoom.hpp"

void ClassRoom::AddStudent(Student student) {
    students_.emplace_back(student);
}

bool ClassRoom::RemoveStudent(const std::string &student_name) {
    auto it = FindStudent(student_name);

    if (it == students_.end()) {
        return false;
    }

    students_.erase(it);
    return true;
}


bool ClassRoom::Search(const std::string &student_name) {
    return (FindStudent(student_name) != students_.end());
}

void ClassRoom::LoadFromFile(const std::string &filename) {
    std::ifstream in;
    in.open(filename);

    std::string line;
    while(std::getline(in, line)) {
        AddStudent(Student{ line });
    }

}

std::vector<Student>::const_iterator ClassRoom::FindStudent(const std::string &student_name) {
    auto it = std::find_if(std::begin(students_)
                         , std::end(students_)
                         , [student_name](const auto &student) {
        return (student.GetName() == student_name);
    });
    return it;
}


void ClassRoom::Print() {
    std::cout << "Class Room" << std::endl;
    std::cout << "==========" << std::endl;
    for(const auto student : students_) {
        std::cout << "\t" << student << std::endl;
    }
    std::cout << std::endl;
}
