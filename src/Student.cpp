#include "Student.hpp"

std::ostream &operator<<(std::ostream &os, const Student &other) {
    os << other.GetName();
    return os;
}

Student::Student(const std::string &name) : name_{name} {
    // Empty
}

void Student::SetName(const std::string &name) {
    name_ = name;
}

const std::string Student::GetName() const {
    return name_;
}

