#pragma once

#include <fstream>
#include <string>
#include <vector>

#include "Student.hpp"

class ClassRoom {
    public:
        void AddStudent(Student student);

        bool RemoveStudent(const std::string &student_name);

        bool Search(const std::string &student_name);

        void LoadFromFile(const std::string &filename);

        void Print();

    private:
        std::vector<Student>::const_iterator FindStudent(const std::string &student_name);

    private:
        std::vector<Student> students_;

};
