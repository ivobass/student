#include <iostream>
#include <memory>
#include <string>

#include "Menu.hpp"
#include "Student.hpp"
#include "ClassRoom.hpp"


int main(int argc, char **argv) {
    std::shared_ptr<ClassRoom> room = std::make_shared<ClassRoom>();
    Menu menu{room};
    menu.Show();
    return EXIT_SUCCESS;
}