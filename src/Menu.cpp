#include <iostream>
#include <string>

#include "Menu.hpp"


Menu::Menu(std::shared_ptr<ClassRoom> room) : room_{room} {
}

void Menu::AddStudentMenuItem() {
    std::string name;
    std::cout << "Adicionar novo estudante, Introduzir o nome: ";
    std::cin >> name;
    room_->AddStudent(Student{name});
}

void Menu::RemoveStudentMenuItem() {
    std::string name;
    std::cout << "Remover estudante, Introduzir nome a remover: ";
    std::cin >> name;
    if (room_->RemoveStudent(name)) {
        std::cout << name << " Eliminado estudante da turma" << std::endl;
    } else {
        std::cout << name << " nao existe na turma" << std::endl;
    }
}

void Menu::SearchStudentMenuItem() {
    std::string name;
    std::cout << "Buscar estudante, introduzir nome: ";
    std::cin >> name;
    bool result = room_->Search(name);
    std::cout << "Estudante " << name << (result ? " " : " not ") << "Encontrado!" << std::endl;
}

void Menu::PrintStudentMenuItem() {
    room_->Print();
}

void Menu::LoadFromFileMenuItem() {
    const std::string kFilename{"Dados.txt"};
    room_->LoadFromFile(kFilename);
}

void Menu::QuitMenuItem() {
    quit_ = true;
}

int Menu::DrawMenu() {
    int option{0};

    std::cout << "Sala de estudantes Menu" << std::endl;
    std::cout << "==============" << std::endl;

    std::cout << "1. Adicionar Estudante" << std::endl;
    std::cout << "2. Remover Estudante" << std::endl;
    std::cout << "3. Encontrar Estudante" << std::endl;
    std::cout << "4. Carregar lista desde um ficheiro txt" << std::endl;

    std::cout << "8. Imprimir Turma de alunos" << std::endl;
    std::cout << "9. Sair" << std::endl;

    std::cout << "Opção: ";
    std::cin >> option;

    return option;
}

void Menu::Show() {
    while(!quit_) {
        int option{DrawMenu()};
        switch(option) {
            case 1:
                AddStudentMenuItem();
                break;
            case 2:
                RemoveStudentMenuItem();
                break;
            case 3:
                SearchStudentMenuItem();
                break;
            case 4:
                LoadFromFileMenuItem();
                break;
            case 8:
                PrintStudentMenuItem();
                break;
            case 9:
                quit_ = true;
                break;
            default:
                std::cout << "Opção invalida, tentar novamente" << std::endl;
        }
    }

    std::cout << "Obrigado por utilizar esta aplicação"
              << std::endl << std::endl;
}
